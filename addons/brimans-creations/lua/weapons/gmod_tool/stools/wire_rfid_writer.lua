TOOL.Category		= "Wire - RFID"
TOOL.Name			= "RFID Writer"
TOOL.Command		= nil
TOOL.ConfigName		= ""
TOOL.Tab			= "Wire"

if ( CLIENT ) then
    language.Add( "Tool.wire_rfid_writer.name", "RFID Writer" )
    language.Add( "Tool.wire_rfid_writer.desc", "Writes values to RFID tags." )
    language.Add( "Tool.wire_rfid_writer.0", "Primary: Write values\nSecondary: Copy values\nReload: Clear values" )
    language.Add( "WireRFIDWriterTool_A", "A:" )
	language.Add( "WireRFIDWriterTool_B", "B:" )
	language.Add( "WireRFIDWriterTool_C", "C:" )
	language.Add( "WireRFIDWriterTool_D", "D:" )
end

TOOL.ClientConVar[ "rfid_a" ] = ""
TOOL.ClientConVar[ "rfid_b" ] = ""
TOOL.ClientConVar[ "rfid_c" ] = ""
TOOL.ClientConVar[ "rfid_d" ] = ""

local function SetRFID( Player, Entity, Data )
	if ( Data and Data._hasRFID and Data._a and Data._b and Data._c and Data._d ) then
		Entity.__RFID_HASRFID = true
		Entity.__RFID_A = tonumber(Data._a)
		Entity.__RFID_B = tonumber(Data._b)
		Entity.__RFID_C = tonumber(Data._c)
		Entity.__RFID_D = tonumber(Data._d)
		duplicator.StoreEntityModifier( Entity, "WireRFID", Data )
	else
		Data = {_hasRFID = false, _a = nil, _b = nil, _c = nil, _d = nil}
		Entity.__RFID_HASRFID = false
		Entity.__RFID_A = nil
		Entity.__RFID_B = nil
		Entity.__RFID_C = nil
		Entity.__RFID_D = nil
		duplicator.StoreEntityModifier( Entity, "WireRFID", Data )
	end
end
duplicator.RegisterEntityModifier( "WireRFID", SetRFID )

function TOOL:LeftClick(trace)
	if (not trace.Entity:IsValid()) then return end
	if (CLIENT) then return end

	local A = self:GetClientInfo("rfid_a")
	local B = self:GetClientInfo("rfid_b")
	local C = self:GetClientInfo("rfid_c")
	local D = self:GetClientInfo("rfid_d")

	SetRFID( Player, trace.Entity, {_hasRFID = true, _a = A, _b = B, _c = C, _d = D} )

	return true
end

function TOOL:Reload(trace)
	if (not trace.Entity:IsValid()) then return end
	if (CLIENT) then return end

	SetRFID( Player, trace.Entity, {_hasRFID = false, _a = nil, _b = nil, _c = nil, _d = nil} )

	return true
end

function TOOL:RightClick(trace)
	if (not trace.Entity:IsValid()) then return end
	if (CLIENT) then return end
	
	local hasRFID = trace.Entity.__RFID_HASRFID
	local A = trace.Entity.__RFID_A
	local B = trace.Entity.__RFID_B
	local C = trace.Entity.__RFID_C
	local D = trace.Entity.__RFID_D
	
	if (not hasRFID or not A or not B or not C or not D) then return end

    self:GetOwner():ConCommand('wire_rfid_writer_rfid_a "' .. A .. '"')
	self:GetOwner():ConCommand('wire_rfid_writer_rfid_b "' .. B .. '"')
	self:GetOwner():ConCommand('wire_rfid_writer_rfid_c "' .. C .. '"')
	self:GetOwner():ConCommand('wire_rfid_writer_rfid_d "' .. D .. '"')
end



function TOOL.BuildCPanel(panel)
	panel:AddControl("Header", { Text = "#Tool.wire_namer.name", Description = "#Tool.wire_namer.desc" })

	panel:AddControl("Slider", {
		Label = "#WireRFIDWriterTool_A",
		Type = "Float",
		Min = "0",
		Max = "10000",
		Command = "wire_rfid_writer_rfid_a"
	})
	
	panel:AddControl("Slider", {
		Label = "#WireRFIDWriterTool_B",
		Type = "Float",
		Min = "0",
		Max = "10000",
		Command = "wire_rfid_writer_rfid_b"
	})
	
	panel:AddControl("Slider", {
		Label = "#WireRFIDWriterTool_C",
		Type = "Float",
		Min = "0",
		Max = "10000",
		Command = "wire_rfid_writer_rfid_c"
	})
	
	panel:AddControl("Slider", {
		Label = "#WireRFIDWriterTool_D",
		Type = "Float",
		Min = "0",
		Max = "10000",
		Command = "wire_rfid_writer_rfid_d"
	})
end
