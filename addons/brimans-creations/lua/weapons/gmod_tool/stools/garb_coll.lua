TOOL.Category		= "Debugging"
TOOL.Name			= "Garbage Collector"
TOOL.Command		= nil
TOOL.ConfigName		= ""

if ( CLIENT ) then
    language.Add( "Tool.garb_coll.name", "Garbage Collector" )
    language.Add( "Tool.garb_coll.desc", "Forces the Lua garbage collector to collect all garbage." )
    language.Add( "Tool.garb_coll.0", "Primary: Collect Garbage\nSecondary: Get Lua Memory Usage" )
	language.Add( "GCMemUsage", "Used Mem. (KB): " )
end

TOOL.ClientConVar[ "memuse" ] = ""

function TOOL:LeftClick(trace)
	collectgarbage("collect")

	return true
end

function TOOL:RightClick(trace)
	local mem = collectgarbage("count")
    self:GetOwner():ConCommand('garb_coll_memuse "' .. mem .. '"')
	
	return true
end



function TOOL.BuildCPanel(panel)
	panel:AddControl("Header", { Text = "#Tool.wire_namer.name", Description = "#Tool.wire_namer.desc" })

	panel:AddControl("Slider", {
		Label = "#GCMemUsage",
		Type = "Float",
		Min = "0",
		Max = "999999999",
		Command = "garb_coll_memuse"
	})
end
