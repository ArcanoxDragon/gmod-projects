
-- MakeSpherical tool made by Dave
-- Thanks to Divran for help busting bugs (and for teaching me how to make tools).
	
-- Version 2012-11-25
-- Updated by DeltaDesignRus to be Garrysmod13 compatible

if CLIENT then
	
	language.Add( "Tool.MakeSpherical.name", "Spherical Collisions" )
	language.Add( "Tool.MakeSpherical.desc", "Gives entities a spherical collisions with a defined radius" )
	language.Add( "Tool.MakeSpherical.0", "Left click to make an entity have spherical collisions based on its size. Right click to set collisions with a custom radius" )
	language.Add( "Tool.MakeSpherical.1", "Left click to make an entity have spherical collisions based on its size. Right click to set collisions with a custom radius" )
	language.Add( "MakeSpherical.radius", "Set radius: " )
	
	function TOOL.BuildCPanel( panel )
		
		panel:AddControl("Header", { Text = "#Tool_MakeSpherical_name", Description = "Tool_MakeSpherical_desc" })
		
		panel:AddControl("Slider", {
			Label = "#MakeSpherical_radius",
			Type = "Float",
			Min = "1",
			Max = "200",
			Command = "MakeSpherical_radius"
		})
	
	end
	
end

TOOL.Category		= "Construction"
TOOL.Name			= "#Make Spherical"
TOOL.ClientConVar[ "radius" ] = "20"

local function IsEntityValid( this )
	
	if !this || !this:IsValid() then return false end
	if SERVER and not this:GetPhysicsObject():IsValid() then return false end
	
	return true
	
end

function MakeSphere(player, entity, data )
	
	if not SERVER then return end
	local physic = entity:GetPhysicsObject()
	
	if data.radius > 1 && data.enabled == true then
		
		local radius = data.radius
		entity:PhysicsInitSphere( radius , physic:GetMaterial() )
		entity:SetCollisionBounds( Vector( -radius, -radius, -radius ), Vector( radius, radius, radius ) )
		physic:Wake()
		if (data.mass and entity:GetPhysicsObject():IsValid()) then entity:GetPhysicsObject():SetMass(data.mass) end
		print("Made entity with mass " .. tostring(data.mass))
		
	end

	duplicator.StoreEntityModifier( entity, "sphere", data )
	
end

duplicator.RegisterEntityModifier( "sphere", MakeSphere )

function TOOL:LeftClick( trace )

	local ent = trace.Entity
	if !IsEntityValid( ent ) then return false end
	local obb = ent:OBBMaxs() - ent:OBBMins()
	
	local data = {}
		data.radius = math.max(obb.x, obb.y, obb.z) / 2
		data.enabled = true
		data.mass = ent:GetPhysicsObject():GetMass()
		
	print("Preparing to make entity with mass " .. tostring(data.mass))

	MakeSphere(self:GetOwner(), ent, data )
	return true
	
end

function TOOL:RightClick( trace )

	local ent = trace.Entity
	if !IsEntityValid( ent ) then return false end
	
	local data = {}
		data.radius = self:GetClientNumber( "radius" )
		data.enabled = true
		data.mass = ent:GetPhysicsObject():GetMass()
	
	MakeSphere(self:GetOwner(), ent, data )
	return true
		
end

function TOOL:Reload( trace )

	local ent = trace.Entity
	if !IsEntityValid( ent ) then return false end
	
	ent:PhysicsInit( SOLID_VPHYSICS )
	ent:SetMoveType( MOVETYPE_VPHYSICS )
	ent:SetSolid( SOLID_VPHYSICS )
	ent:GetPhysicsObject():Wake()
	
	local data = {}
		data.radius = 0
		data.enabled = false
		data.mass = ent:GetPhysicsObject():GetMass()
	
	MakeSphere(self:GetOwner(), ent, data )
	return true
	
end
