@name AdjustableValue
@inputs Up Down
@outputs Value
interval(10)

Min = 0
Max = 100
Chg = 1

if (Up > 0)
{
    Value += Chg
}

if (Down > 0)
{
    Value -= Chg
}

Value = max(min(Value, Max), Min)
