@name DataConverter
@inputs InFeet InSource
@outputs OutFeet OutSource
@trigger all

OutFeet = toUnit("ft", InSource)
OutSource = fromUnit("ft", InFeet)
