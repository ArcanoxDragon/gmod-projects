@name ATCController3
#EStop: >1 to disable system
#Start: >1 to set speed to 15 and start system
#Entity: train bogie to measure speed of
@inputs EStop Start DoorsClosed Entity:entity
#RState: The State of the RFID reader
#A, B, C, D: The values read from the RFID reader
@inputs RState A B C D
#GPSX, GPSY, GPSZ: The GPS coordinates
@inputs GPSX GPSY GPSZ
#CurSpeed: The current calculated speed of the train
#Speed: The speed to pass to the train drive controller
#PBrake: Sets the parking brake so the wheels lock up (hold at station)
#Doors: This gets fired once every time the train stops
@outputs CurSpeed Speed PBrake RDoors RAction LDoors LAction
#SetSpeed: The current speed for the train to achieve
#State: The current state of the state machine
#Ticks: The general-purpose variable used for timing
@outputs SetSpeed State Ticks NextState Distance
@persist SetSpeed State Ticks NextState Distance StartPos:vector MaxDist Started
@persist LDEnable RDEnable
@trigger RState
runOnTick(1)

#ESTOP_RANGE = 1900
#SPEED_RANGE = 90
#S_WAIT_TIME = 5
#ESTOP_WTIME = 2
#SPD_STOPPED = 0.02

### Global Values
GTable = gTable("train")
#ESTOP_RANGE = GTable["EStop_Range",number]
#SPEED_RANGE = GTable["Speed_Range",number]
#CLEAR_RANGE = GTable["Clear_Range",number]
#S_WAIT_TIME = GTable["S_Wait_Time",number]
#S_DOOR_TIME = GTable["S_Door_Time",number]
#ESTOP_WTIME = GTable["EStop_WTime",number]
#SPD_STOPPED = GTable["Spd_Stopped",number]
#STOP_SEARCH_SPD = GTable["stopSearchSpd",number]
#Global_EStop = GTable["Global_EStop",number]
STOP_SEARCH_SPD = 1.0
S_DOOR_TIME = 2.5
S_WAIT_TIME = 10.0
Global_EStop = 0.0

### Constants
S_RUNNING = 0
S_STOPPING = 1
S_WAITING = 2
S_DOORS_OPENING = 3
S_DOORS_CLOSING = 4
S_STOPPED = 5
S_OFF = 6

### Timing
function number tps()
{
    return 100
}

### Speed Calc
CurSpeed = toUnit("mph", entity():velL():y())

### Init
if (Start > 0)
{
    if (Started <= 0)
    {
        SetSpeed = 20
        State = S_RUNNING
        Started = 1
    }
}
else
{
    if (Started > 0)
    {
        Started = 0
        State = -1
        SetSpeed = 0
    }
}

### Throttle
if (State < 0) #Invalid
{
    State = S_RUNNING
}
elseif (State == S_RUNNING) #Running normally
{
    LDEnable = 0
    RDEnable = 0
    RDoors = 0
    LDoors = 0
    RAction = 0
    LAction = 0
    PBrake = 0
    if (Ticks <= 0)
    {
        Ticks = 0
        if (RState > 0) #Detected speed plate
        {
            if (C == 3) #Normal speed plate
            {
                SetSpeed = B
            }
            elseif (C == 1) #Station signal
            {
                MaxDist = B
                StartPos = vec(GPSX, GPSY, GPSZ)
                State = S_STOPPING
                Ticks = tps()
            }
        }
    }
    else
    {
        Ticks = Ticks - 1
    }
    Speed = SetSpeed
}
elseif (State == S_STOPPING)
{
    if (RState > 0 && C == 4) #Park train
    {
        Ticks = 0
        NextState = S_OFF
        State = S_OFF
        SetSpeed = 0
        Speed = 0
    }
    else
    {
        Ticks = Ticks - 1
        if (Ticks < 0) { Ticks = 0 }
        if (Ticks <= 0 && RState > 0 && C == 2)
        {
            SetSpeed = B
            Distance = 0
            Speed = 0
            PBrake = 1
            Ticks = tps()
            if (D == 1)
            {
                RDEnable = 0
                LDEnable = 1
            }
            elseif (D == 2)
            {
                RDEnable = 1
                LDEnable = 1
            }
            else
            {
                RDEnable = 1
                LDEnable = 0
            }
            NextState = S_DOORS_OPENING
            State = S_WAITING
        }
        else
        {
            CurPos = vec(GPSX, GPSY, GPSZ)
            Distance = toUnit("ft", CurPos:distance(StartPos))
            Progress = Distance / MaxDist
            Speed = min(max((1 - Progress) * SetSpeed, STOP_SEARCH_SPD), SetSpeed)
        }
    }
}
elseif (State == S_WAITING)
{
    if (Ticks > 0)
    {
        Ticks = Ticks - 1
    }
    else
    {
        Ticks = 0
        State = NextState
    }
}
elseif (State == S_DOORS_OPENING)
{
    if (RDEnable > 0)
    {
        RDoors = 1
        RAction = 1
    }
    if (LDEnable > 0)
    {
        LDoors = 1
        LAction = 1
    }
    Ticks = S_DOOR_TIME * tps()
    NextState = S_STOPPED
    State = S_WAITING
}
elseif (State == S_STOPPED)
{
    LDoors = 0
    RDoors = 0
    LAction = 0
    RAction = 0
    Ticks = S_WAIT_TIME * tps()
    NextState = S_DOORS_CLOSING
    State = S_WAITING
}
elseif (State == S_DOORS_CLOSING)
{
    if (RDEnable > 0)
    {
        RDoors = 1
        RAction = 1
    }
    if (LDEnable > 0)
    {
        LDoors = 1
        LAction = 1
    }
    soundPlay("doorsClosing", soundDuration("train/doors_closing.wav"), "train/doors_closing.wav")
    soundVolume("doorsClosing", 0.25)
    Ticks = S_DOOR_TIME * tps() + tps()
    NextState = S_RUNNING
    State = S_WAITING
}

if (EStop + Global_EStop > 0 || DoorsClosed <= 0)
{
    Speed = 0
    PBrake = 1
}
