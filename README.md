Briman's Garry's Mod Projects

Installation:

Copy the "data" and "addons" folders to your %SteamApps%\common\GarrysMod\garrysmod\ folder, merging them with whatever might already be there. This should be all you need to do for installation.

Other addons (I used these while creating my projects. Addons marked with a * are most likely required to use my projects):

- *Advanced Duplicator
- *WireMod
- *Wire Extras (including my patch)
-  Precision Tool
- *ACF Weight Tool
-  Measuring Stick
- *No-Collide World
-  SmartSnap
-  Make Spherical (including my patch, which is actually a new version of the whole addon...)

My patches are found in the /patches folder in the root of the repository. Install the contents of this folder *over* (after) the other addons. For make-spherical, you will need to use my patch *instead* of the addon found on the workshop.

Using the projects:

Auto-Subway:
	The automatic subway is similar to the MagLev but uses wheels and a guiderail instead of zero-friction sliders. It is much smoother than the maglev and is the only project currently being updated. Load the "gm_autosubway" map and spawn the autosubway/stationary_subway.txt and autosubway/train_accelchair.txt dupes (at original locations/angles) with the Advanced Duplicator. The front chair of the train is connected to the GUI and the pod controller. You may need to wire the "EGP:wirelink" input of the EGP controller to the actual EGP controller (they're right next to each other at the front of the train) for the GUI to show up. The train does not currently have doors. The controls are the same as the MagLev. The train will stop itself at each station and maintain a safe speed based on the speed zone nodes.
	
	NOTE: THIS USES A PATCHED VERSION OF THE MAKE-SPHERICAL ADDON AND REQUIRES MY PATCH TO THE WIRE-EXTRAS ADDON AS WELL. THE PATCHES ARE IN THE REPO AND MUST BE INSTALLED OVER THE ADDONS FOR THEM TO WORK CORRECTLY.

MagLev:
	To use the MagLev, load up the "gm_flatgrass_maglev" map in Garry's Mod. Use the Advanced Duplicator tool to load the "maglev/stationary.txt" dupe and place it in the world (make sure the "Spawn at original location" and "Spawn at original angles" boxes are CHECKED). Do the same procedure for the "maglev/train.txt" dupe. Unfreeze the train and get into the seat at the front. The controls are as follows (and only work when you're in the driver seat, allowing for multiple trains on the map at once):
	
	R - Toggle between Automatic / Manual mode
	
	Automatic Mode:
		In this mode, the train drives completely autonomously. You only need to enter the driver seat and press "R" to activate it. You can leave the seat at any time and the train will continue to operate. It will stop at all stations and follow the speed limit designated by the pads on the left side of the track.
		
	Manual Mode:
		In manual mode, the train is completely under your control. Use these controls to drive it:
			W/S - Adjust the throttle between 100 (max power forwards) and -100 (max power backwards)
			F - Return the throttle to 0 (coast maintaining speed)
			Space - Toggle the parking brake.
			Left Mouse - Open/Close the left-side doors
			Right Mouse - Open/Close the right-side doors
		Driving tips:
			The train is quite heavy and will not accelerate/brake very fast. Act early when braking to make sure you don't fly past stations or around corners. Use negative power to slow down the train to about 1MPH and then apply the parking brake (as a real electric train will do). Don't go around corners or slope transitions (the slope itself is fine) at more than about 20-25MPH or you may end up crashing the train (or the physics engine, or Garry's Mod itself...). Slow down to about 15MPH before the train reaches the start of a station platform to ensure you can stop in time.
	Troubleshooting:
		If the train will not move in either mode, make sure you have spawned the "stationary" dupe from the "maglev" folder. This contains a chip that specifies default physics properties. If you can find the chip, you can modify them :). If it moves in manual mode but not in automatic mode, you probably left a door open. Enter manual mode and open/re-close all doors.